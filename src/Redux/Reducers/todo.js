import { createSlice } from "@reduxjs/toolkit";
import canPromise from "qrcode/lib/can-promise";

export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    todolist: [],
  },
  reducers: {
    addTodo: (state, action) => {
      state.todolist.push(action.payload);
    },
    deleteTodo: (state, action) => {
      state.todolist.splice(action.payload, 1);
    },
    editTodos: (state, action) => {
      state.todolist[action.payload[0]] = action.payload[1];
    },
  },
});

export const { addTodo, deleteTodo, editTodos } = todoSlice.actions;

export default todoSlice.reducer;
