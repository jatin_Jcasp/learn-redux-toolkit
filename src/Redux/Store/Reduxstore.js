import { configureStore } from "@reduxjs/toolkit";
import todo from "../Reducers/todo";

export const store = configureStore({
  reducer: {
    todo: todo,
  },
});
