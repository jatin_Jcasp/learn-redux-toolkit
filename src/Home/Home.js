import React, { useState } from "react";
import classes from "../styles/Home.module.css";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, deleteTodo, editTodos } from "../Redux/Reducers/todo";

const Home = () => {
  const [onUpdate, setOnUpdate] = useState(-1);
  const dispatch = useDispatch();
  const { register, handleSubmit, reset, setValue } = useForm();
  const todos = useSelector((state) => state.todo.todolist);
  let i = 0;
  const onsubmit = (data, e) => {
    if (data.todo && data.AssignTo) {
      const todolist = {
        todo: data.todo,
        AssignTo: data.AssignTo,
        Status: data.Status,
      };
      if (onUpdate == -1) {
        dispatch(addTodo(todolist));
      } else {
        dispatch(editTodos([onUpdate, todolist]));
        setOnUpdate(-1);
      }
    }
  };

  const editTodo = (id) => {
    setValue("todo", todos[Number(id)].todo);
    setValue("AssignTo", todos[Number(id)].AssignTo);
    setValue("Status", todos[Number(id)].Status);
    setOnUpdate(Number(id));
  };

  return (
    <>
      <div>
        <form className={classes.form} onSubmit={handleSubmit(onsubmit)}>
          <div className={classes.div}>
            <div>
              <label>Enter your Todo: </label>
            </div>
            <input
              type="text"
              {...register("todo", {
                required: true,
              })}
            />
          </div>
          <div className={classes.div}>
            <div>
              <label>Assign to: </label>
            </div>
            <input
              type="text"
              {...register("AssignTo", {
                required: true,
              })}
            />
          </div>
          <div className={classes.div}>
            <div>
              <label>Todo status: </label>
            </div>
            <select
              {...register("Status", {
                required: true,
              })}
            >
              <option>Planned</option>
              <option>In-progress</option>
              <option>Test on Development</option>
              <option>Test on Production</option>
              <option>Cancelled</option>
            </select>
          </div>
          <button className={classes.button} onClick={onsubmit}>
            {onUpdate == -1 ? "Add" : "Update"}
          </button>
        </form>
      </div>
      <div className={classes.home}>
        {todos[0] ? (
          <ul>
            <p className={classes.p}>My Todo list:</p>
            <table border="1">
              <thead>
                <tr>
                  <th>Todo</th>
                  <th>Assign To</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {todos.map((todo) =>
                  todo.todo ? (
                    <>
                      <tr>
                        <td>{`${todo.todo}`}</td>
                        <td>{`${todo.AssignTo}`}</td>
                        <td>{`${todo.Status}`}</td>
                        <td>
                          <button
                            id={i}
                            onClick={(e) => {
                              editTodo(e.target.id);
                            }}
                          >
                            Edit
                          </button>
                          <button
                            onClick={(e) => {
                              dispatch(deleteTodo(e.target.className));
                            }}
                            className={i++}
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    </>
                  ) : (
                    ""
                  )
                )}
              </tbody>
            </table>
          </ul>
        ) : (
          <p className={classes.p2}>No todos.</p>
        )}
      </div>
    </>
  );
};

export default Home;
