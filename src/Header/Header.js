import React from "react";
import header from "../styles/Header.module.css";
import Navigation from "./Navigation/Navigation";

const Header = () => {
  return (
    <div className={header.header}>
      <h2 className={header.heading}>TODO App</h2>
    </div>
  );
};

export default Header;
